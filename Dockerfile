FROM requarks/wiki:latest

# Replace with your email address:
ENV WIKI_ADMIN_EMAIL romain@pop.eu.com

WORKDIR /var/wiki

# Replace your-config.yml with the path to your config file:
ADD config.yml config.yml

VOLUME /var/wiki

EXPOSE 3000
ENTRYPOINT [ "node", "server" ]